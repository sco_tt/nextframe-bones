			<footer class="footer" role="contentinfo">

				<div id="inner-footer" class="wrap clearfix">

			<?php if ( is_active_sidebar( 'sidebar-5' ) ) : ?>
				<?php dynamic_sidebar( 'sidebar-5' ); ?>
			<?php endif; ?>
	
				</div>

			</footer>

		</div>

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>

	</body>

</html>
