/*
Bones Scripts File
Author: Eddie Machado

This file should contain any js scripts you want to add to the site.
Instead of calling it in the header or throwing it inside wp_head()
this file will be called automatically in the footer so as not to
slow the page load.

*/

// IE8 ployfill for GetComputed Style (for Responsive Script below)
if (!window.getComputedStyle) {
	window.getComputedStyle = function(el, pseudo) {
		this.el = el;
		this.getPropertyValue = function(prop) {
			var re = /(\-([a-z]){1})/g;
			if (prop == 'float') prop = 'styleFloat';
			if (re.test(prop)) {
				prop = prop.replace(re, function () {
					return arguments[2].toUpperCase();
				});
			}
			return el.currentStyle[prop] ? el.currentStyle[prop] : null;
		}
		return this;
	}
}

// as the page loads, call these scripts
jQuery(document).ready(function($) {

	/*
	Responsive jQuery is a tricky thing.
	There's a bunch of different ways to handle
	it, so be sure to research and find the one
	that works for you best.
	*/
	
	/* getting viewport width */
	var responsive_viewport = $(window).width();
	
	/* if is below 481px */
	if (responsive_viewport < 481) {
	
	} /* end smallest screen */
	
	/* if is larger than 481px */
	if (responsive_viewport > 481) {
	
	} /* end larger than 481px */
	
	/* if is above or equal to 768px */
	if (responsive_viewport >= 768) {
	
		/* load gravatars */
		$('.comment img[data-gravatar]').each(function(){
			$(this).attr('src',$(this).attr('data-gravatar'));
		});
		
	}
	
	/* off the bat large screen actions */
	if (responsive_viewport > 1030) {
	
	}
	
	
	// add all your scripts here

/**
Unslider 
**/

/* First Try
** Code from http://jsfiddle.net/UmtkY/
** Wasn't working because height wasn't being set before first image loaded
**/

/* COMMENT OUT FIRST TRY

//begin unslider
$(function() {
    $('.banner').unslider({
	speed: 500,               //  The speed to animate each slide (in milliseconds)
	delay: 3000,              //  The delay between slide animations (in milliseconds)
	complete: function() {},  //  A function that gets called after every slide animation
	keys: false,               //  Enable keyboard (left, right) arrow shortcuts
	fluid: true,               //  Support responsive design. May break non-responsive designs
	arrows: true,
});
}); //end unslider

COMMENT OUT FIRST TRY */

/* Take Two
** From http://css-tricks.com/forums/topic/unslider-simple-jquery-slider-not-working/#post-158772
**/





//begin unslider
//hide the banner so that users don't see the images until the slider is initialized

$('.banner').hide();

var callUnslider = function(){

    $('.banner').show();
  //   $('.banner').removeClass('loading');

    var unslider = $('.banner').unslider({
        speed : 500,                //  The speed to animate each slide (in milliseconds)
        delay : 7000,               //  The delay between slide animations (in milliseconds)
        complete : function() {},   //  A function that gets called after every slide animation
        keys : false,                //  Enable keyboard (left, right) arrow shortcuts
        fluid : true,               //  Support responsive design. May break non-responsive designs
        arrows: true
    });

}

$('.unslider-arrow').click(function() {
    var fn = this.className.split(' ')[1];

    //  Either do unslider.data('unslider').next() or .prev() depending on the className
    unslider.data('unslider')[fn]();
});


//if IE caches the images, it will never fire a load event for it
//here we add a query string to the first banner image's src attribute, so that IE will not cache the image.

//get the image's src attribute
var source = $('.banner ul li img').first().attr("src");
//add the date as a query string to the image's src attribute, so that every time the page is refreshed, IE will reload it and fire the load event
$('.banner ul li img').first().attr("src",source + "?" + new Date().getTime());
//initilize the unslider plugin, but only after the first image loads
$('.banner ul li img').first().load(function(){callUnslider ()});

//end unslider



/*
** end unslider
**/


	//Responsive Nav
  $('body').addClass('js');
  var $menu = $('#menu'),
    $menulink = $('.menu-link');
  
$menulink.click(function() {
  $menulink.toggleClass('menu-link-touch');
  $menu.toggleClass('active');
  return false;
});




 
}); /* end of as page load scripts */


/*! A fix for the iOS orientationchange zoom bug.
 Script by @scottjehl, rebound by @wilto.
 MIT License.
*/
(function(w){
	// This fix addresses an iOS bug, so return early if the UA claims it's something else.
	if( !( /iPhone|iPad|iPod/.test( navigator.platform ) && navigator.userAgent.indexOf( "AppleWebKit" ) > -1 ) ){ return; }
	var doc = w.document;
	if( !doc.querySelector ){ return; }
	var meta = doc.querySelector( "meta[name=viewport]" ),
		initialContent = meta && meta.getAttribute( "content" ),
		disabledZoom = initialContent + ",maximum-scale=1",
		enabledZoom = initialContent + ",maximum-scale=10",
		enabled = true,
		x, y, z, aig;
	if( !meta ){ return; }
	function restoreZoom(){
		meta.setAttribute( "content", enabledZoom );
		enabled = true; }
	function disableZoom(){
		meta.setAttribute( "content", disabledZoom );
		enabled = false; }
	function checkTilt( e ){
		aig = e.accelerationIncludingGravity;
		x = Math.abs( aig.x );
		y = Math.abs( aig.y );
		z = Math.abs( aig.z );
		// If portrait orientation and in one of the danger zones
		if( !w.orientation && ( x > 7 || ( ( z > 6 && y < 8 || z < 8 && y > 6 ) && x > 5 ) ) ){
			if( enabled ){ disableZoom(); } }
		else if( !enabled ){ restoreZoom(); } }
	w.addEventListener( "orientationchange", restoreZoom, false );
	w.addEventListener( "devicemotion", checkTilt, false );
})( this );

