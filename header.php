<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<?php // Google Chrome Frame for IE ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title><?php wp_title(''); ?></title>

		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

		<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<?php // or, set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php // wordpress head functions ?>
		<?php wp_head(); ?>
		<?php // end of wordpress head ?>
		<script>window.jQuery || document.write('<script src="/wp-content/themes/nextFrame-bones/library/js/jquery.min.js"><\/script>')</script>
		</script>
		<script src="//cdn.jsdelivr.net/jquery.event.move/1.3.5/jquery.event.move.min.js"></script>
		<script src="//cdn.jsdelivr.net/jquery.event.swipe/0.5.2/jquery.event.swipe.min.js"></script>
		<!-- <script src="//cdn.jsdelivr.net/jquery.unslider/0.1/unslider.js"></script> -->
		<script src="/wp-content/themes/nextFrame-bones/library/js/unslider-jsd.min.js"></script>
		<script type="text/javascript" src="//use.typekit.net/qgg0url.js"></script>
		<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
		

		<?php // drop Google Analytics Here ?>
		<?php // end analytics ?>

	</head>

	<body <?php body_class(); ?>>

		<div id="container">

			<header class="header" role="banner">

				<div id="inner-header" class="wrap clearfix">

					<?php // to use a image just replace the bloginfo('name') with your img src and remove the surrounding <p> ?>
					<div id="logo" class="h1 col span_1_of_3">
						<a href="
						<?php echo home_url(); ?>" rel="nofollow">
						<div class="hide"> <?php bloginfo('name'); ?></div>
						<img src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" alt="<?php bloginfo('name'); ?>" class="top-logo" width="100%" height="200px">
						</a>
					</div>

					<?php // if you'd like to use the site description you can un-comment it below ?>
					<?php // bloginfo('description'); ?>

						<a href="#menu" class="menu-link"><div class="menu-icon-wrapper"><i class="fa fa-reorder"></i><i class="fa fa-times"></i></div></a>
						<nav id="menu" role="navigation" class="">
						<?php bones_main_nav(); ?>
					</nav>
				</div>


			</header>


			<?php if ( is_active_sidebar( 'slider-widget' ) ) : ?>
				<?php dynamic_sidebar( 'slider-widget' ); ?>
			<?php endif; ?>
