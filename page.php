<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

						<div id="main" class="twelvecol first clearfix section group" role="main">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix col span_2_of_3' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<header class="article-header">

								<?php if(is_front_page() ) { ?>
								<div class="hide">
								<?php } ?>

									<h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>

								<?php if(is_front_page() ) { ?>
								</div>
								<?php } ?>

								</header>

								<section class="entry-content clearfix" itemprop="articleBody">
									<?php //Don't change this, it will override the static front page content
									the_content(); ?>
							</section>

								<footer class="article-footer">
									<?php the_tags( '<span class="tags">' . __( 'Tags:', 'bonestheme' ) . '</span> ', ', ', '' ); ?>

								</footer>

								<?php comments_template(); ?>

							</article>

							<?php endwhile; else : ?>

									<article id="post-not-found" class="hentry clearfix">
										<header class="article-header">
											<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
										<section class="entry-content">
											<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the page.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>
							<?php 
								//Pullin our custom sidebar defined in template.php
									if ( ! dynamic_sidebar( 'sidebar1' ) ) : 
								?>
							<?php endif; // end sidebar widget area ?>
							<?php //get_sidebar(); ?>
						</div>


				</div>

			</div>

<?php get_footer(); ?>
